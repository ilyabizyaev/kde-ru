---
title: Приложения KDE
permalink: /apps
layout: default
# SEO
image: img/apps.png
description: ! 'Сообщество KDE разрабатывает более 200 приложений: диспетчер файлов, текстовые и графические редакторы, браузер и офисный пакет, обучающие программы и многое другое.'
---

# Приложения KDE
![Иконки приложений](img/kirigami-devices.png){: .float-right style="width: 200px;"}
Сообщество KDE разрабатывает более двухсот приложений: диспетчер файлов, текстовые и графические редакторы, браузер и офисный пакет, обучающие программы и многое другое. Благодаря использованию кроссплатформенного фрейворка [Qt](https://www.qt.io) и основанных на нём библиотек [KDE Frameworks](https://www.kde.org/develop), наши программы работают на всех популярных операционных системах.  
[Полный список приложений](https://www.kde.org/applications/){: .learn-more .button}

## <i class="fab fa-linux pr-2" style="color: #7f8c8d;"></i> GNU/Linux и <i class="fab fa-freebsd px-2" style="color: #7f8c8d;"></i> FreeBSD
Все приложения KDE доступны для операционных систем GNU/Linux и FreeBSD. Для установки программ используйте центр приложений (например, [Discover](https://userbase.kde.org/Discover)), пакетный менеджер вашего дистрибутива или [пакеты Flatpak](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak).  
[Дистрибутивы GNU/Linux с приложениями KDE](distributions){: .learn-more .button}
## <i class="fab fa-windows pr-2" style="color: #7f8c8d;"></i> Windows
Для ОС Windows доступны стабильные сборки следующих программ:

<div class="row">
<div class="col-md">
{% include app-link.html icon="falkon.svg"   descr="Легковесный веб-браузер Falkon"         get="https://www.falkon.org/download/#windows"       web="https://www.falkon.org" %}
{% include app-link.html icon="krita.svg"    descr="Растровый графический редактор Krita"   get="https://krita.org/en/download/krita-desktop/"   web="https://krita.org/" %}
{% include app-link.html icon="kdenlive.svg" descr="Нелинейный видеоредактор Kdenlive"      get="https://kdenlive.org/en/download/"              web="https://kdenlive.org/" %}
{% include app-link.html icon="kate.svg"     descr="Мощный текстовый редактор Kate"         get="https://kate-editor.org/get-it/"                web="https://kate-editor.org/" %}
{% include app-link.html icon="kdevelop.svg" descr="Среда разработки KDevelop"              get="https://www.kdevelop.org/download"              web="https://www.kdevelop.org/" %}
{% include app-link.html icon="kmymoney.png"      descr="Менеджер личных финансов KMyMoney"      get="https://kmymoney.org"                           web="https://kmymoney.org" %}
{% include app-link.html icon="gcompris.png" descr="Набор развивающих игр GCompris" get="https://gcompris.net/downloads-ru.html#windows" web="https://gcompris.net/" %}
</div>

<div class="col-md">
{% include app-link.html icon="kile.svg"     descr="Редактор TeX/LaTeX Kile"         get="https://kile.sourceforge.io/download.php"         web="https://kile.sourceforge.io/" %}
{% include app-link.html icon="umbrello.svg" descr="Редактор UML Umbrello"           get="https://download.kde.org/stable/umbrello/latest/" web="https://umbrello.kde.org" %}
{% include app-link.html icon="rkward.svg"   descr="Интерфейс к языку R RKWard"      get="https://rkward.kde.org/Windows"                   web="https://rkward.kde.org/" %}
{% include app-link.html icon="kexi.svg"     descr="Редактор баз данных Kexi"      get="http://kexi-project.org/wiki/wikiview/index.php@Download.html" web="http://kexi-project.org" %}
{% include app-link.html icon="marble.svg"   descr="Виртуальный глобус Marble"       get="https://marble.kde.org/install.php"               web="https://marble.kde.org/" %}
{% include app-link.html icon="digikam.svg"  descr="Система управления фото digiKam" get="https://www.digikam.org/download/binary/#Windows" web="https://www.digikam.org/" %}
</div>
</div>

*Windows® является зарегистрированной торговой маркой корпорации Microsoft в США и других странах.*

## <i class="fab fa-apple pr-2" style="color: #7f8c8d;"></i> macOS
Несколько проектов предоставляют бинарные сборки для macOS.  
*macOS является товарным знаком компании Apple Inc., зарегистрированным в США и других странах.*  
[Перейти к загрузке](https://community.kde.org/Mac){: .learn-more .button}
## <i class="fab fa-android pr-2" style="color: #7f8c8d;"></i> Android
Большинство приложений KDE для Android распространяются централизованно через официальный аккаунт сообщества в Google Play.  
![Скриншот каталога](img/apps-play.png){: .img-fluid .rounded .header-image}  
[Перейти к загрузке](https://play.google.com/store/apps/dev?id=4758894585905287660){: .learn-more .button}

Исключение составляет GCompris, для которого отдельно выпускается платная версия. Это не нарушает лицензию и позволяет авторам посвящать больше времени работе над программой.  

<div class="row">
<div class="col-md-6">
{% include app-link.html icon="gcompris.png" descr="GCompris для Android" get="https://play.google.com/store/apps/developer?id=Timothée+Giet" web="https://gcompris.net/" %}
</div>
</div>

*Android является товарным знаком компании Google Inc. Google Play и логотип Google Play являются товарными знаками корпорации Google LLC.*

{% include back-button.html link="get" %}
