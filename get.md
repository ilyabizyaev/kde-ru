---
title: Загрузить
permalink: /get
layout: default
pre-content: plasma-desktop
# SEO
image: img/kirigami-devices.png
description: ! 'Скачать программное обеспечение KDE: Plasma и приложения'
---

# Приложения KDE
![Иконки приложений](img/apps.png){: .float-right}
Сообщество KDE разрабатывает более двухсот приложений: диспетчер файлов, текстовые и графические редакторы, браузер и офисный пакет, обучающие программы и многое другое.  
[Скачать приложения KDE](apps){: .learn-more .button .d-inline-block}

{% include app-carousel.html %}
# Plasma Mobile
![Смартфон с Plasma Mobile](img/phone.png){: .float-right}
Plasma Mobile — открытая мобильная графическая оболочка от KDE. Она поддерживает темы и виджеты Plasma и большинство программ для ОС Linux. Встроенные приложения построены с использованием библиотеки адаптивных компонентов интерфейса Kirigami.  
Plasma Mobile может быть установлена на устройства с ОС Android, адаптированные участниками [Halium](https://github.com/halium/projectmanagement/issues?q=is:issue is:open label:Ports) или [postmarketOS](https://wiki.postmarketos.org/wiki/Devices).  
[Перейти на сайт проекта](https://www.plasma-mobile.org){: .learn-more .button .d-inline-block}

