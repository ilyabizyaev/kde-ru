---
title: Дистрибутивы
permalink: /distributions
layout: default
# SEO
image: img/laptop.png
description: ! 'Популярные дистрибутивы GNU/Linux c предустановленным программным обеспечением KDE'
---

# Дистрибутивы с Plasma и приложениями KDE
Ниже представлены несколько популярных дистрибутивов GNU/Linux c предустановленным программным обеспечением KDE. Выбор дистрибутива персонален и опирается на ваши предпочтения. Рекомендуем ознакомиться с описаниями на сайтах проектов, чтобы получить более полное представление.

## KDE neon
![Логотип KDE neon](img/distros/logos/neon.svg){: .distribution-logo}
KDE neon предоставляет последние версии рабочей среды и приложений KDE со стандартными настройками поверх стабильной основы Ubuntu с длительным сроком поддержки (LTS).  
Доступен в нескольких изданиях: для пользователей (User Edition), стабильные сборки для разработчиков (Developer Edition Git-Stable) и экспериментальные сборки (Developer Edition Git-Unstable).  
[Перейти на сайт](https://neon.kde.org){: .learn-more .button .d-inline-block}  
![Скриншот KDE neon](img/distros/bg/neon.png){: .img-fluid .rounded .header-image}

## openSUSE
![Логотип openSUSE](img/distros/logos/opensuse.svg){: .distribution-logo}
openSUSE представлен двумя вариантами:  
• [Leap](https://software.opensuse.org/distributions/leap) — стабильный дистрибутив с регулярными выпусками, поставляется с LTS-версиями Linux, Qt и Plasma  
• [Tumbleweed](https://software.opensuse.org/distributions/tumbleweed) — rolling-дистрибутив с последними версиями всех пакетов  
[Перейти на сайт](https://www.opensuse.org){: .learn-more .button .d-inline-block}  
![Скриншот openSUSE](img/distros/bg/opensuse.png){: .img-fluid .rounded .header-image}

## Kubuntu
![Логотип Kubuntu](img/distros/logos/kubuntu.svg){: .distribution-logo}
Kubuntu — версия Ubuntu с программным обеспечением KDE по умолчанию. Разработчики стремятся к обеспечению удобства в использовании для начинающих. В установку по умолчанию включены многие популярные программы и утилита для управления драйверами.  
[Перейти на сайт](https://kubuntu.org){: .learn-more .button .d-inline-block}  
![Скриншот Kubuntu](img/distros/bg/kubuntu.png){: .img-fluid .rounded .header-image}

## Manjaro KDE
![Логотип Manjaro](img/distros/logos/manjaro.svg){: .distribution-logo}
Manjaro — дистрибутив, основанный на [Arch Linux](https://www.archlinux.org), нацеленный на упрощение его установки и настройки. Версия Manjaro KDE, в частности, настроена для использования KDE Plasma и базовых приложений KDE.  
[Перейти на сайт](https://manjaro.org){: .learn-more .button .d-inline-block}  
![Скриншот Manjaro](img/distros/bg/manjaro.png){: .img-fluid .rounded .header-image}

## Альт Рабочая станция К
![Логотип Альт Рабочая станция К](img/distros/logos/alt.svg){: .distribution-logo}
Независимый российский дистрибутив с KDE. Отдельно доступна [образовательная версия](https://www.basealt.ru/products/alt-education/), включённая в Единый реестр российских программ для ЭВМ и баз данных.  
[Перейти на сайт](https://www.basealt.ru/products/alt-workstation/skachat/){: .learn-more .button .d-inline-block}  
![Скриншот Альт Рабочая станция К](img/distros/bg/alt.png){: .img-fluid .rounded .header-image}

## Другие дистрибутивы
Многие другие дистрибутивы GNU/Linux используют ПО KDE или предлагают возможность его установки. Полный список доступен на [KDE Community Wiki](https://community.kde.org/Distributions).

{% include back-button.html link="get" %}
