/* Active page highlighting */
$(document).ready(function() {
  $('li.active').removeClass('active');
  $('a[href="' + location.href + '"]').closest('li.nav-item').addClass('active'); 
});

/* Language detection */
$(document).ready(function() {
    if (['ru', 'ru-RU', 'uk', 'uk-UA', 'be', 'be-BY'].indexOf(navigator.language) < 0) {
        $('#differentBrowserLanguage').show();
    }
});

/* Back to top */
// When the user scrolls down 100px from the top of the document, show the button
function scrollFunction() {
    if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
        document.getElementById("beamUp").style.display = "block";
    } else {
        document.getElementById("beamUp").style.display = "none";
    }
}

window.onscroll = function() {scrollFunction()};

function scrollTo(element, to, duration) {
    var start = element.scrollTop,
        change = to - start,
        currentTime = 0,
        increment = 20;
        
    var animateScroll = function(){        
        currentTime += increment;
        var val = Math.easeInOutQuad(currentTime, start, change, duration);
        element.scrollTop = val;
        if(currentTime < duration) {
            setTimeout(animateScroll, increment);
        }
    };
    animateScroll();
}

//t = current time
//b = start value
//c = change in value
//d = duration
Math.easeInOutQuad = function (t, b, c, d) {
    t /= d / 2;
    if (t < 1) return c / 2 * t * t + b;
    t--;
    return -c / 2 * (t * (t - 2) - 1) + b;
};

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    scrollTo(document.documentElement, 0, 500);
    document.getElementById("beamUp").blur();
}
