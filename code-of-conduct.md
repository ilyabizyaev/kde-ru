---
title: Правила поведения в KDE
permalink: /code-of-conduct
layout: default
# SEO
image: img/kde-icon-192x192.png
description: ! 'Этот документ содержит рекомендации, позволяющие обеспечить эффективную атмосферу для совместной работы участников KDE'
---

# Правила поведения в KDE

## Предисловие
В сообществе KDE участники со всего мира объединяются для создания свободного программного обеспечения. Это возможно благодаря поддержке, усердному труду и энтузиазму тысяч людей, в особенности тех, кто создаёт и использует программное обеспечение KDE.

Этот документ содержит рекомендации, позволяющие обеспечить эффективную, позитивную и вдохновляющую атмосферу для совместной работы участников KDE, и объясняет, как мы можем укрепить и поддержать друг друга.

Данные правила поведения соблюдаются всеми участниками, а также пользователями, взаимодействующими с участниками и сервисами сообщества.

## Общие сведения
Правила поведения представляют собой краткое изложение ценностей и «здравого смысла» сообщества. Основные социальные устои, скрепляющие проект:

* Будьте вежливы
* Уважайте других
* Сотрудничайте
* Будьте прагматичны
* Поддерживайте других
* Принимайте поддержку

Наше сообщество состоит из многих групп людей и организаций, которые могут быть примерно поделены на:

* Участников, то есть тех, кто вносит вклад в проект, улучшая программное обеспечение и сервисы KDE
* Пользователей, то есть тех, кто помогает проекту как потребители готовых продуктов

Эти правила поведения отражают согласованные стандарты поведения участников сообщества при любом общении: на форумах, в чатах и в списках рассылки, на каналах IRC, вики-страницах, сайтах, при публичных встречах и личном общении в контексте KDE. Сообщество действует в соответствии со стандартами, описанными в данных правилах, и будет защищать их во благо сообщества. Лидеры любых групп: модераторы списков рассылки, чатов, каналов IRC, форумов и др. — могут осуществлять право приостанавливать доступ тем, кто регулярно нарушает наши правила поведения.

## Будьте вежливы
Ваши действия влияют на других людей, ваша работа используется ими, а вы в свою очередь зависите от работы и действий других. Любое принятое решение повлияет на других членов сообщества, и от вас ожидается осознание этих последствий при принятии решений.

Как участник, убедитесь, что в полной мере цените работу других, и помните, как вносимые вами изменения влияют на других. От вас также ожидается следование расписанию разработки и соответствующим правилам.

Как пользователь, помните, что участники усердно работают над своей частью продуктов KDE и гордятся этим. Если вы разочарованы или раздражены, не забывайте, что ваши проблемы, скорее всего, могут быть разрешены при вежливом предоставлении точной информации всем заинтересованным лицам.

## Уважайте других
Чтобы сообщество KDE оставалось здоровым, его участники должны чувствовать себя комфортно. Уважительное отношение друг к другу абсолютно необходимо для этого. В случае разногласий в первую очередь предполагайте, что люди желают добра.

Мы не терпим персональных нападок, расизма, сексизма и других форм дискриминации. Разногласия время от времени неизбежны, однако уважение взглядов других поможет завоевать уважение собственной позиции. Уважение других людей, их работы и вклада, а также предположение лучших побуждений помогут участникам чувствовать себя комфортно и в безопасности, улучшат мотивацию и продуктивность.

Мы ожидаем, что члены сообщества будут с уважением относиться к другим участникам, пользователям и сообществам. Помните, что KDE — международное сообщество, а вы можете не знать о важных аспектах других культур.

## Сотрудничайте
Движение свободного программного обеспечения опирается на взаимодействие. Взаимодействие позволяет избежать дублирования усилий и повысить качество продуктов. Чтобы избежать непонимания, постарайтесь ясно и точно излагать свои мысли при запросе и предоставлении помощи. Помните, что очень легко неверно истолковать электронные сообщения (особенно написанные не на вашем родном языке). В случае сомнения уточняйте; помните первое правило: предполагайте, что люди желают добра.

Как разработчик, вы должны стремиться к сотрудничеству с другими членами сообщества, а также с другими сообществами, заинтересованными в вашей работе. Ваша работы должна быть прозрачной и возвращаться к сообществу по мере доступности, а не только во время релизов. Если вы хотите работать над чем-то новым в существующих проектах, сообщайте этим проектам о своих идеях, планах и успехах.

Не всегда получается достичь консенсуса в выборе способа реализации идеи, поэтому не чувствуйте себя обязанным найти его до начала работы. Однако всегда убеждайтесь, что окружающие знают о вашей работе, и публикуйте её так, чтобы они могли обсудить, протестировать и улучшить её.

Участники всех проектов приходят и уходят. Когда вы частично или полностью покидаете проект, делайте это с гордостью за свои достижения и с ответственным отношением к тем, кому предстоит продолжить начатое вами.

Как пользователь, знайте, что ваш отзыв важен, но не менее важна его форма. Необдуманные комментарии могут причинить боль и лишить мотивации других членов сообщества, в то время как вдумчивое обсуждение проблем может принести положительные результаты. Поощрение творит чудеса.

## Будьте прагматичны
KDE — прагматичное сообщество. Мы ценим ощутимые результаты больше, чем последнее слово в обсуждении. Мы защищаем наши основополагающие ценности, такие как свобода и взаимное уважение, но не даём спорам о незначительных проблемах встать на пути к достижению более значимых результатов. Мы открыты для предложений и рады принять решения вне зависимости от их происхождения. В случае сомнений предпочитайте решение, которое поможет достичь цели, тому, у которого есть теоретические преимущества, но нет развития. Используйте инструменты и способы, которые помогут в работе. Позволяйте решениям приниматься теми, кто работает.

## Поддерживайте других
Наше сообщество сильно благодаря взаимному уважению, взаимодействию и прагматичному, ответственному поведению. Иногда эти устои приходится защищать: другим членам сообщества нужна помощь.

Если вы стали свидетелем нападок на кого-то, подумайте, как лично вы можете помочь им. Если же чувствуете, что ситуация вне вашего контроля, в частном порядке обратитесь к жертве и узнайте, требуется ли официальное вмешательство. Аналогично требуется поддерживать всех, кто находится под угрозой истощения из-за стресса, связанного с работой или личными проблемами.

Когда проблемы возникают, обдумайте вежливое напоминание вовлечённым о наших правилах поведения как о первой помощи. Лидеров определяют действия, и они могут подать хороший пример, работая над разрешением проблем в соответствии с этими правилами до того, как проблемы обострятся.

## Принимайте поддержку
Разногласия, как политические, так и технические, случаются часто, и наше сообщество — не исключение. Наша цель — не избежать несогласия или разнообразия взглядов, а разрешить их конструктивно. Вы должны обращаться к сообществу за помощью и разрешением разногласий, а где возможно, совещаться с наиболее задействованной командой.

Хорошо подумайте перед превращением несогласия в общественный спор. Если это необходимо, запросите посредничество, чтобы решить несогласие менее эмоциональным путём. Если вы считаете, что на вас или вашу работу нападают, остановитесь и сделайте несколько спокойных вдохов перед тем, как писать разгорячённые ответы. Если используются чрезмерно эмоциональные фразы, рассмотрите 24-часовой мораторий (перерыв) как выход из ситуации: иногда всё, что нужно, это охладить прения. Если вы всё же хотите сделать по-своему, мы поощряем публикацию ваших идей и вашей работы для испытания и тестирования остальными.

Этот документ доступен под лицензией Creative Commons Attribution — Share Alike 3.0.

Авторы правил хотели бы поблагодарить сообщество KDE, тех, кто принимал участие в создании условий для публикации этого документа, и тех, кто делился своими мыслями при написании. Кроме того, они выражают благодарность другим сообществам (таким как сообщество Ubuntu), правила которых использовались при формировании данных.

__Примечание переводчика:__ данный документ является близким к оригиналу переводом [KDE Community Code of Conduct](https://www.kde.org/code-of-conduct/) на русский язык. В случаях расхождения толкований данного перевода с оригиналом приоритет отдаётся оригинальному тексту.
